package main

import (
	"hello/model"
	"hello/routes"
)

func main() {
	router := routes.Route()
	model.Connect()
	router.Run()
}
