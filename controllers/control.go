package controllers

import (
	"hello/model"

	"github.com/gin-gonic/gin"
)

func Hello(c *gin.Context) {
	c.JSON(200, gin.H{"Message": "Hello"})
}

func ShowStudent(c *gin.Context) {
	student, err := model.FindStudent()
	if err != nil {
		panic(err)
	}
	c.JSON(200, student)
}

func ImportStudent(c *gin.Context) {
	student := model.Student{
		StudentID: "A",
		Name:      "1",
	}
	err := model.InsertStudent(student)
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.JSON(200, gin.H{"Bool": "Success"})
}
