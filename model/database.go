package model

import (
	"log"
	"os"

	mgo "gopkg.in/mgo.v2"
)

var db *mgo.Database

type Student struct {
	StudentID string `bson:"studentid"`
	Name      string `bson:"name"`
}

func Connect() {
	session, err := mgo.Dial("mongo:27017")
	if err != nil {
		log.Fatalln(err)
		log.Fatalln("mongo err")
		os.Exit(1)
	}
	session.SetMode(mgo.Monotonic, true)

	db = session.DB("Test")
}

func FindStudent() ([]Student, error) {
	var student []Student

	err := db.C("Student").Find(nil).All(&student)
	if err != nil {
		return []Student{}, err
	}

	return student, nil

}

func InsertStudent(student Student) error {

	err := db.C("Student").Insert(student)
	if err != nil {
		return err
	}
	return nil
}
