FROM golang:latest


ENV GO111MODULE=on
COPY . /go/src/hello
WORKDIR /go/src/hello

RUN go get ./
RUN go get github.com/pilu/fresh

EXPOSE 8080
CMD ["fresh","-c","fresh.conf"]