package routes

import (
	"hello/controllers"

	"github.com/gin-gonic/gin"
)

func Route() *gin.Engine {
	router := gin.Default()

	router.GET("/", controllers.Hello)
	router.GET("/find", controllers.ShowStudent)
	router.POST("/insert", controllers.ImportStudent)
	return router
}
